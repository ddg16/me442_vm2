from __future__ import print_function
from six.moves import input
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list
#Above lines have all necessary imports to interface with ROS and some neccesary math functions
#Create ROS node and Moveit Commander Link
moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("move_group_python_interface_tutorial", anonymous=True)
#Create robot object as well as a scene variable for the robot's environment
robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()
#Create joint group for ur5 robot
group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)
#Command to display trajectory in RVis
display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)
#Move Robot to more ideal Starting Configuration
tau = 2.0 * pi #declare angle of 360 degrees for reference
joint_goal = move_group.get_current_joint_values()
joint_goal[0] = 0
joint_goal[1] = -tau / 8
joint_goal[2] = 0
joint_goal[3] = -tau / 4
joint_goal[4] = 0
joint_goal[5] = tau / 6  # 1/6 of a turn


#MOVE TO TOP OF VERTICAL LINE OF "d"
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.681
pose_goal.position.y = 0.311
pose_goal.position.z = 0.575
#Move the Robot to the desired position
move_group.set_pose_target(pose_goal)
#Boolean that makes sure the robot reached the desired position
success = move_group.go(wait=True)
#End all robot movement
move_group.stop()
#Clear move target so next position doesn't overlap with old one
move_group.clear_pose_targets()


#MOVE TO BOTTON OF VERTICAL LINE OF "d"
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.744
pose_goal.position.y = 0.294
pose_goal.position.z = 0.356
#Move the Robot to the desired position
move_group.set_pose_target(pose_goal)
#Boolean that makes sure the robot reached the desired position
success = move_group.go(wait=True)
#End all robot movement
move_group.stop()
#Clear move target so next position doesn't overlap with old one
move_group.clear_pose_targets()

#MOVE TO BOTTON LEFT OF BULB OF "d"
pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.0
pose_goal.position.y = 0.0
pose_goal.position.z = 0.0
#Move the Robot to the desired position
move_group.set_pose_target(pose_goal)
#Boolean that makes sure the robot reached the desired position
success = move_group.go(wait=True)
#End all robot movement
move_group.stop()
#Clear move target so next position doesn't overlap with old one
move_group.clear_pose_targets()




